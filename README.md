# buteo

## What is Buteo?
The idea of Buteo is to build an XMPP based Groupware application.
An application for collaborative work on Debian project.

## What is XMPP?
XMPP is a **E**xtensible **M**essaging and **P**resence **P**rotocol.

 * RFC 6120: Extensible Messaging and Presence Protocol (XMPP): Core
 * RFC 6121: Extensible Messaging and Presence Protocol (XMPP): Instant Messaging and Presence
 * RFC 7395: An Extensible Messaging and Presence Protocol (XMPP) Subprotocol for WebSocket
 * RFC 7590: Use of Transport Layer Security (TLS) in the Extensible Messaging and Presence Protocol (XMPP)
 * RFC 7622: Extensible Messaging and Presence Protocol (XMPP): Address Format
