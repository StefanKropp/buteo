/*!
 * \file buteo.cpp
 *
 *
 */
#include "buteo.hpp"

using namespace buteo;

/*!
 * \brief Start Buteo Application
 */
void Buteo::start() { BOOST_LOG_TRIVIAL(info) << "Started " << PACKAGE_STRING; }
