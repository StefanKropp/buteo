// vim: expandtab:ts=4:sts=4:sw=4

/*!
 *
 * \file main.cpp
 *
 * \mainpage 
 *
 * Buteo - A XMPP based Groupware Application
 */

#include "buteo.hpp"
#include "common.hpp"

#include <cstdlib>
#include <gtkmm.h>

int main(int argc, char *argv[]) {
  BOOST_LOG_TRIVIAL(info) << "Starting " << PACKAGE_STRING;
  auto app = Gtk::Application::create(argc, argv, "debian.buteo");

  Glib::RefPtr<Gtk::Builder> builder =
      Gtk::Builder::create_from_file("data/buteo.glade");
  Gtk::ApplicationWindow *appWin = nullptr;
  builder->get_widget("ButeoApplicationWindow", appWin);

  buteo::Buteo b;

  appWin->signal_show().connect(sigc::mem_fun(b, &buteo::Buteo::start));
  return app->run(*appWin);
}
