// vim: expandtab:ts=4:sts=4:sw=4
#ifndef _BUTEO_HPP__
#define _BUTEO_HPP__

#include "common.hpp"

namespace buteo {

class Buteo {
public:
  void start();
};
}; // namespace buteo

#endif // _BUTEO_HPP__
